import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Components
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatGridListModule} from '@angular/material/grid-list';

import { MatIconModule } from '@angular/material/icon';

import { AllTableComponent } from './src/all-table/all-table.component';
import { DefaultTableComponent } from './src/default-table/default-table.component';
import { SummaryTableComponent } from './src/summary-table/summary-table.component';
import { CountriesTableComponent } from './src/countries-table/countries-table.component';
import { ByCountryTableComponent } from './src/by-country-table/by-country-table.component';
import { DayOneTableComponent } from './src/day-one-table/day-one-table.component';
import { CountryStatusTableComponent } from './src/country-status-table/country-status-table.component';

// Service
import { Covid19ApiService } from './services/covid19api.service';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [
    AppComponent,
    AllTableComponent,
    DefaultTableComponent,
    SummaryTableComponent,
    CountriesTableComponent,
    ByCountryTableComponent,
    DayOneTableComponent,
    CountryStatusTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    
    HttpClientModule,

    MatProgressSpinnerModule,
    MatInputModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatGridListModule,
    MatButtonModule
  ],
  exports: [
    MatInputModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatGridListModule,
    MatButtonModule
  ],
  providers: [Covid19ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
