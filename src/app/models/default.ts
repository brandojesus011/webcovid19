export interface DefaultI {
    /*
        "Name": "Add a webhook to be notified when new data becomes available",
        "Description": "POST Request must be in JSON format with key URL and the value of the webhook. Response data is the same as returned from /summary",
        "Path": "/webhook",
        "Params": [
            "URL",
            "webhook"
        ]
    */
   Name: string;
   Description: string;
   Path: string;
   Params: any | null;
}
