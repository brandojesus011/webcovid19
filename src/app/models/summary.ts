export interface SummaryI {
    Countries: CountriesI[];
    Date: string;
}

export interface CountriesI {
    Country: string;
    Slug: string;
    NewConfirmed: number;
    TotalConfirmed: number;
    NewDeaths: number;
    TotalDeaths: number;
    NewRecovered: number;
    TotalRecovered: number;
}