import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

import { DefaultI } from '../models/default';
import { SummaryI } from '../models/summary';

@Injectable({
  providedIn: 'root'
})
export class Covid19ApiService {
  baseUrl = environment.baseUrl;

  constructor(
    private http: HttpClient
  ) { }

  getDefault(): Observable<DefaultI[]> {
    return this.http.get<DefaultI[]>(`${this.baseUrl}`);
  }

  getSummary(): Observable<SummaryI> {
    return this.http.get<SummaryI>(`${this.baseUrl}/summary`);
  }
}
