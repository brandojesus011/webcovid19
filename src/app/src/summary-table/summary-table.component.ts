import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

import { Subject } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';

import { Covid19ApiService } from '../../services/covid19api.service';
import { SummaryI, CountriesI } from 'src/app/models/summary';

@Component({
  selector: 'app-summary-table',
  templateUrl: './summary-table.component.html',
  styleUrls: ['./summary-table.component.scss']
})
export class SummaryTableComponent implements OnInit {
  $unsubscribe: Subject<void> = new Subject<void>();

  displayedColumns: string[] = ['Country', 'NewConfirmed', 'TotalConfirmed', 'NewDeaths', 'TotalDeaths', 'NewRecovered', 'TotalRecovered'];
  dataSource: MatTableDataSource<CountriesI>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  length = 10;
  pageSize = 5;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  constructor(
    private covid19ApiService: Covid19ApiService
  ) { }

  ngOnInit(): void {
    this.getSummary();
  }

  getSummary() {
    this.covid19ApiService.getSummary()
    .pipe(
      takeUntil(this.$unsubscribe)
    )
    .subscribe(response => {
      console.info('res ', response);
      const summary = response.Countries;
      const countries = summary.slice(1, summary.length);

      this.dataSource = new MatTableDataSource<CountriesI>(countries);
      this.dataSource.paginator = this.paginator;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
