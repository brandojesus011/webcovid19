import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DayOneTableComponent } from './day-one-table.component';

describe('DayOneTableComponent', () => {
  let component: DayOneTableComponent;
  let fixture: ComponentFixture<DayOneTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DayOneTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DayOneTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
