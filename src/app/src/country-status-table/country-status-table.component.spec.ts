import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryStatusTableComponent } from './country-status-table.component';

describe('CountryStatusTableComponent', () => {
  let component: CountryStatusTableComponent;
  let fixture: ComponentFixture<CountryStatusTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountryStatusTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryStatusTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
