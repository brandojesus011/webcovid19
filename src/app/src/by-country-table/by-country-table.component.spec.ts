import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByCountryTableComponent } from './by-country-table.component';

describe('ByCountryTableComponent', () => {
  let component: ByCountryTableComponent;
  let fixture: ComponentFixture<ByCountryTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByCountryTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByCountryTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
