import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Covid19ApiService } from '../../services/covid19api.service';
import { DefaultI } from 'src/app/models/default';

@Component({
  selector: 'app-default-table',
  templateUrl: './default-table.component.html',
  styleUrls: ['./default-table.component.scss']
})
export class DefaultTableComponent implements OnInit {
  $unsubscribe: Subject<void> = new Subject<void>();

  displayedColumns: string[] = ['Name', 'Description', 'Path', 'Params'];
  dataSource: MatTableDataSource<DefaultI>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  length = 10;
  pageSize = 5;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  constructor(
    private covid19ApiService: Covid19ApiService
  ) { }

  ngOnInit(): void {
    this.getDefault();
  }

  getDefault() {
    this.covid19ApiService.getDefault()
    .pipe(takeUntil(this.$unsubscribe))
    .subscribe(response => {
      console.info('res ', response);
      this.dataSource = new MatTableDataSource<DefaultI>(response);
      this.dataSource.paginator = this.paginator;
    });
  }

}
