import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultTableComponent } from './src/default-table/default-table.component';
import { SummaryTableComponent } from './src/summary-table/summary-table.component';


const routes: Routes = [
  { path: '', redirectTo: 'default', pathMatch: 'full' },
  { path: 'default', component: DefaultTableComponent },
  { path: 'summary', component: SummaryTableComponent },
  { path: '**', redirectTo: 'default', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
